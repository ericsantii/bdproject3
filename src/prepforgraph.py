import pandas as pd


def forgraph(csv):
    name = csv[:-4]
    model = pd.read_csv(csv)
    model = model.dropna()
    columns = ["Does","Does not","Ambiguous"]

    flu_does_counter = 0
    flu_does_not_counter = 0
    flu_ambiguous_counter = 0

    zika_does_counter = 0
    zika_does_not_counter = 0
    zika_ambiguous_counter = 0

    dia_does_counter = 0
    dia_does_not_counter = 0
    dia_ambiguous_counter = 0

    ebola_does_counter = 0
    ebola_does_not_counter = 0
    ebola_ambiguous_counter = 0

    head_does_counter = 0
    head_does_not_counter = 0
    head_ambiguous_counter = 0

    measles_does_counter = 0
    measles_does_not_counter = 0
    measles_ambiguous_counter = 0

    for row in model.itertuples(index=True, name='model1'):
        if "flu" in getattr(row, "text"):
            if getattr(row,"sentiment") == 1:
                flu_does_counter += 1
            elif getattr(row, "sentiment") == 0:
                flu_does_not_counter +=1
            elif getattr(row, "sentiment") == 2:
                flu_ambiguous_counter +=1
        if "zika" in getattr(row, "text"):
            if getattr(row, "sentiment") == 1:
                zika_does_counter += 1
            elif getattr(row, "sentiment") == 0:
                zika_does_not_counter += 1
            elif getattr(row, "sentiment") == 2:
                zika_ambiguous_counter += 1
        if "diarrhea" in getattr(row, "text"):
            if getattr(row, "sentiment") == 1:
                dia_does_counter += 1
            elif getattr(row, "sentiment") == 0:
                dia_does_not_counter += 1
            elif getattr(row, "sentiment") == 2:
                dia_ambiguous_counter += 1
        if "ebola" in getattr(row, "text"):
            if getattr(row, "sentiment") == 1:
                ebola_does_counter += 1
            elif getattr(row, "sentiment") == 0:
                ebola_does_not_counter += 1
            elif getattr(row, "sentiment") == 2:
                ebola_ambiguous_counter += 1
        if "headache" in getattr(row, "text"):
            if getattr(row, "sentiment") == 1:
                head_does_counter += 1
            elif getattr(row, "sentiment") == 0:
                head_does_not_counter += 1
            elif getattr(row, "sentiment") == 2:
                head_ambiguous_counter += 1
        if "measles" in getattr(row, "text"):
            if getattr(row, "sentiment") == 1:
                measles_does_counter += 1
            elif getattr(row, "sentiment") == 0:
                measles_does_not_counter += 1
            elif getattr(row, "sentiment") == 2:
                measles_ambiguous_counter += 1

    flu_df = pd.DataFrame([[flu_does_counter,flu_does_not_counter,flu_ambiguous_counter]],columns=columns)
    flu_df.to_csv(name+"flu.csv")

    zika_df = pd.DataFrame([[zika_does_counter,zika_does_not_counter,zika_ambiguous_counter]],columns=columns)
    zika_df.to_csv(name+"zika.csv")

    dia_df = pd.DataFrame([[dia_does_counter,dia_does_not_counter,dia_ambiguous_counter]],columns=columns)
    dia_df.to_csv(name+"dia.csv")

    ebola_df = pd.DataFrame([[ebola_does_counter,ebola_does_not_counter,ebola_ambiguous_counter]],columns=columns)
    ebola_df.to_csv(name+"ebola.csv")

    head_df = pd.DataFrame([[head_does_counter,head_does_not_counter,head_ambiguous_counter]],columns=columns)
    head_df.to_csv(name+"head.csv")

    measles_df = pd.DataFrame([[measles_does_counter,measles_does_not_counter,measles_ambiguous_counter]],columns=columns)
    measles_df.to_csv(name+"measles.csv")

def main():
    forgraph("model1.csv")
    forgraph("model2.csv")

if __name__ == '__main__':
    main()