import pandas as pd
import tensorflow as tf
from tensorflow import keras

import numpy as np
from keras.utils import to_categorical


ml_dictionary = dict()
train = pd.read_csv('docs/data/training.csv', header=None, names=['text', 'sentiment'])
test = pd.read_csv('docs/data/parsed_tweets.csv')

counter = 0
for i in range(0, len(test.Tweet)):
    try:
        for word in test.Tweet[i].split(" "):
            if word not in ml_dictionary:
                ml_dictionary[word] = counter
                counter+=1
    except:
        pass

for i in range(0, len(train.text)):
    for word in train.text[i].split(" "):
        if word not in ml_dictionary:
            ml_dictionary[word] = counter
            counter+=1




indexed_test = list()
for i in range(0, len(test.Tweet)):
    try:
        currTweet = list()
        for word in test.Tweet[i].split(" "):
            currTweet.append(ml_dictionary[word])
        indexed_test.append(currTweet)
    except:
        test.drop(i)



indexed= list()
labels= list()
for i in range(0, len(train.text)):
    currTweet = list()
    for word in train.text[i].split(" "):
        currTweet.append(ml_dictionary[word])
    indexed.append(currTweet)
    labels.append(train.sentiment[i])




maximum_length=0

for data in indexed_test:
    maximum_length = max(maximum_length, len(data))

for data in indexed:
    maximum_length = max(maximum_length, len(data))


train_data = keras.preprocessing.sequence.pad_sequences(indexed,
                                                        maxlen=maximum_length)
test_data = keras.preprocessing.sequence.pad_sequences(indexed_test,
                                                       maxlen=maximum_length)

labels = to_categorical(labels)

val_data = train_data[:2000]
train_data = train_data[2000:]

val_labels = labels[:2000]
train_labels = labels[2000:]
for p in train_labels: print(p)

vocab_size=len(ml_dictionary)


model = keras.Sequential()
model.add(keras.layers.Embedding(vocab_size, 16))
model.add(keras.layers.GlobalAveragePooling1D())
model.add(keras.layers.Dense(16, activation=tf.nn.relu))
model.add(keras.layers.Dense(3, activation=tf.nn.softmax))

model.compile(optimizer=tf.train.AdamOptimizer(),
              loss='categorical_crossentropy',
              metrics=['accuracy'])


model.fit(np.array(train_data),
                    np.array(train_labels),
                    epochs=35,
                    batch_size=512,
          validation_data=(np.array(val_data), np.array(val_labels)),
                    verbose=1)

modelResults = model.predict_classes(np.array(test_data))


model2 = keras.Sequential()
model2.add(keras.layers.Flatten())
model2.add(keras.layers.Dense(16, activation=tf.nn.relu))
model2.add(keras.layers.Dense(3, activation=tf.nn.softmax))

model2.compile(optimizer=tf.train.AdamOptimizer(),
              loss='categorical_crossentropy',
              metrics=['accuracy'])


model2.fit(np.array(train_data),
                    np.array(train_labels),
                    epochs=35,
                    batch_size=512,
          validation_data=(np.array(val_data), np.array(val_labels)),
                    verbose=1)

model2Results = model2.predict_classes(np.array(test_data))


model1ResultsDF = pd.DataFrame(modelResults, columns=["sentiment"])
model1ResultsDF['text'] = test


model1ResultsDF.to_csv("docs/data/model1.csv", index=False)

model2ResultsDF = pd.DataFrame(model2Results, columns=["sentiment"])
model2ResultsDF['text'] = test


model2ResultsDF.to_csv("docs/data/model2.csv", index=False)