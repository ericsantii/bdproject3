function parseData(createGraph) {
	Papa.parse("../data/model2zika.csv", {
		download: true,
		complete: function(results) {
			createGraph(results.data);
		}
	});
}

function createGraph(data) {
var chart = c3.generate({
    data: {
        // iris data from R
        columns: [
            ['Does', data[1][1]],
            ['Does not',data[1][2]],
            ['Ambiguous',data[1][3]],
        ],
        type : 'pie',
        onclick: function (d, i) { console.log("onclick", d, i); },
        onmouseover: function (d, i) { console.log("onmouseover", d, i); },
        onmouseout: function (d, i) { console.log("onmouseout", d, i); }
    }
});

}

parseData(createGraph);